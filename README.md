# Bluesky Feed Creation Memo

## Linux distribution

```
$ cat /etc/debian_version
12.4
```

## Install NPM

```
$ su
# apt update
# apt upgrade -y
# apt autoremove -y
# apt install npm
# npm install -g n
# n latest
# npm install -g npm
# exit
```

Open new shell.

```
$ node --version
v21.5.0
$ npm --version
10.2.5
```

## Install packages

```
$ npm install
```

## Gitignore things

Add following code to `.gitignore`:

```
# Misc

yarn.lock
package-lock.json
```

Commands:

```
$ git rm -f yarn.lock
$ git add .
$ git commit -m 'Update .gitignore'
$ git push
```

## Write src/subscription.ts

See https://gitlab.com/hakabahitoyo/bluesky-feed-war-ja.

## Smoke test

```
$ npm run build
```

## Write .env


```
$ cp .env.example .env
```

See https://gitlab.com/hakabahitoyo/bluesky-feed-war-ja.

See also https://bsky.social/xrpc/com.atproto.identity.resolveHandle?handle=pussypharmacy.bsky.social.

```
$ git add -f .env
$ git commit -m 'Write .env'
$ git push
```

## Smoke test 2

```
$ npm run start
```

Stop daemon to push Ctrl + c.

## Deploy

Write `/etc/caddy/Caddyfile`:

```
blueskyfeedwarja.fubaiundo.org {
        reverse_proxy :3000
}
```

Commands:

```
$ su
# systemctl restart caddy
# exit
```

In `screen` command:

```
$ npm run start
```

## Write scripts/publishFeedGen.ts

See https://gitlab.com/hakabahitoyo/bluesky-feed-war-ja.

## Publish

```
$ npm run publishFeed
```

## Bugfix

![Error message](war-ja-2-bugfix-01.png)

```
$ cd src
$ cd algos
$ cp whats-alf.ts war-ja-2.ts
```

Modify `src/algos/index.ts` and `src/algos/war-ja-2.ts`. See also https://gitlab.com/hakabahitoyo/bluesky-feed-war-ja/-/commit/bc8cf92d1b15e87d18e35a6525c2c1df40f9050c.


## その後

`/etc/caddy/Caddyfile`

```
blueskyfeedwarja.fubaiundo.org {
        reverse_proxy localhost:3000
}
```

`localhost` を付けずにただの `:3000` だと動かなくなった。なぜだ？？？？？？
